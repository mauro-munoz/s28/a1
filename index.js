//GET ALL
fetch(`https://jsonplaceholder.typicode.com/todos`)
.then(data => data.json())
.then(data => console.log(data))
//GET ALL and make array of titles
fetch(`https://jsonplaceholder.typicode.com/todos`)
.then(data => data.json())
.then(data => {
    const titleonly = data.map((data) =>  data.title )
    console.table(titleonly)
})

//GET 1 and put into a string
fetch(`https://jsonplaceholder.typicode.com/todos/1`)
.then(data => data.json())
.then(data => {console.log(data)
console.log(`Title is ${data.title} and status is ${data.completed}`)})

fetch(`https://jsonplaceholder.typicode.com/todos`,{
    method: `POST`,
    headers: { 
                'Content-Type': 'application/json'},
     body: JSON.stringify({ 
                completed: true,
                title: "new post using POST",
                userId: 1
            })
})
.then(data => data.json())
.then(data => console.log(data))

//PUT
fetch(`https://jsonplaceholder.typicode.com/todos/2`,{
    method: `PUT`,
    headers: { 
                'Content-Type': 'application/json'},
     body: JSON.stringify({ 
                completed: true,
                title: "new post using PUT method",
                userId: 1,
                Description : "something to put",
                Datacompleted: "12.12.12",
                status : "unopened"
            })
})
.then(data => data.json())
.then(data => console.log(data))

// PATCH
fetch(`https://jsonplaceholder.typicode.com/todos/3`,{
    method: `PATCH`,
    headers: { 
                'Content-Type': 'application/json'},
     body: JSON.stringify({ 
                title: "new post using PATCH method",
                completed: true,
                datecompleted: "12/12/12"
            })
})
.then(data => data.json())
.then(data => console.log(data))


//DELETE
fetch(`https://jsonplaceholder.typicode.com/todos/4`,{
    method:`DELETE`
})